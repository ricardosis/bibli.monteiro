@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			@include('alerts.validation')
			<div class="card">
				<div class="card-header">Novo Livro</div>

				<div class="card-body">
					{!! Form::model($livro, ['route' => ['livros.update', $livro ], 'files' => true, 'method' => 'put', 'id' => 'edit-form']) !!}   
					<div class="form-group">
						<label>Titulo</label>
						{!! Form::text('titulo',null, ['id' => 'titulo', 'class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						<label>Genero</label>
						{!! Form::select('genero',array(
						'matematica'       => 'Matemática'   
						),null,['id' => 'genero', 'class' => 'form-control', 'placeholder' => 'Selecione um gênero']) !!}
					</div>
					<div class="form-group">
						<label>Editora</label>
						{!! Form::text('editora',null, ['id' => 'editora', 'class' => 'form-control']) !!}
					</div>        
					<div class="form-group">
						<label>Autor</label>
						{!! Form::text('autor',null, ['id' => 'autor', 'class' => 'form-control']) !!}
					</div>  
					<div class="form-group">
						<label>Quantidade</label>
						{!! Form::number('quantidade',null, ['id' => 'qnt', 'class' => 'form-control']) !!}
					</div>  
					<div class="form-group">
						<label>Imagem</label>
						{!! Form::file('img',null) !!}
					</div> 
					{!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()