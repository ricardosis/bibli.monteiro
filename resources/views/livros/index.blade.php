@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			@include('alerts.messages')
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-6">Todos Livros</div>
						<div class="col-md-6 text-right">
							<a href="{{ route('livros.create') }}" type="button" class="btn btn-primary btn-sm pull-right">Novo Livro</a>
						</div>
					</div>
				</div>

				<div class="card-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Titulo</th>
								<th>Genero</th>
								<th>Editora</th>
								<th>Autor</th>
								<th>Quantidade</th>
								<th>Imagem</th>
								<th>Ação</th>
							</tr> 
						</thead>
						@foreach ($livros as $livro )
						<tbody>
							<tr>
								<td>
									{{ $livro->titulo }}
								</td>
								<td>
									{{ $livro->genero }}
								</td>
								<td>
									{{ $livro->editora }}
								</td>
								<td>
									{{ $livro->autor }}
								</td>
								<td>
									{{ $livro->quantidade }}
								</td>
								<td>
									<img src="{{ asset('storage/images/'.$livro->img) }}" width="25px" height="50px" />
								</td>
								<td class="actions">
									<a href="{{ route('livros.edit', ['id' => $livro->id ]) }}" class="btn btn-sm btn-primary">Editar</a>
									{!!Form::open(['route'=> ['livros.destroy',$livro], 'method' => 'DELETE'])!!}
									{!! Form::submit('Deletar', ['class' => 'btn btn-sm btn-danger']) !!}
									{!! Form::close() !!}
								</td>
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
