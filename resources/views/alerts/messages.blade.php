@if (Session::has('success'))
    <div class="alert alert-success">
        <p>
            <p>{{ Session::get('success') }}</p>
        </p>
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        <p>
           <p>{{ Session::get('error') }}</p> 
        </p>
    </div>
@endif