<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livros extends Model
{
    //

    protected $table = 'livros';
    
	protected $fillable = ['titulo', 'genero', 'editora', 'autor', 'quantidade','img','path'];

	public $timestamps = false;
}
