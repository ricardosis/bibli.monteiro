<?php

namespace App\Http\Controllers\Livro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Livros;
use Illuminate\Support\Facades\Storage;
use File;

class LivroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $livros = Livros::all();

        return view('livros.index', compact(['livros']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('livros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'titulo'    => 'required',
            'genero'    => 'required',
            'editora' => 'required',
            'autor'     => 'required',
            //'img'       => 'required|mimes:jpeg,jpg,png|max:1000',
            'quantidade'  => 'numeric|required|between:1,999'
        ];

        $this->validate($request, $rules);
        $livro = $request->all();
        
        Livros::create($livro);
        
        return redirect()->route('livros.index')->with('success', 'Livro adicionado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $livro = Livros::findOrFail($id);
        return view('livros.edit', compact('livro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $livro = Livros::findOrFail($id);

        $rules = [
            'titulo'    => 'required',
            'genero'    => 'required',
            'editora' => 'required',
            'autor'     => 'required',
            //'img'       => 'required|mimes:jpeg,jpg,png|max:1000',
            'quantidade'  => 'numeric|required|between:1,999'
        ];

        $this->validate($request, $rules);

        $livro->update();

        return redirect()->route('livros.index')->with('success', 'Livro atualizado com sucesso.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $livro = Livros::findOrFail($id);
        $livro->delete();

        return redirect()->route('livros.index')->with('success', 'Livro removido com sucesso.');
    }
}
